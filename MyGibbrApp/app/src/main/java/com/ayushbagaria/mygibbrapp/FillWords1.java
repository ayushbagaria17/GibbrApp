package com.ayushbagaria.mygibbrapp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FillWords1 extends AppCompatActivity implements View.OnClickListener {
    String sampleText;
    String []myStrings;
    ArrayList<String> blanks;
    TextView headerTv, footerTv;
    EditText input;
    Button okBtn;
    String str1 = " word(s) left", str2 = "please type a/an ";
    int position = 1;
    String[] answers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_words1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        input = (EditText)findViewById(R.id.input);
        okBtn = (Button)findViewById(R.id.ok_btn);
        okBtn.setOnClickListener(this);
        headerTv = (TextView) findViewById(R.id.tv_count);
        footerTv= (TextView) findViewById(R.id.tv_footer);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            sampleText = extras.getString("mySampleText");
        }

        String delimiters = "\\<[a-zA-Z-]*\\>";
        myStrings = sampleText.split(delimiters);
        blanks = new ArrayList<>();
        Pattern p = Pattern.compile("\\<(.*?)\\>");
        Matcher m = p.matcher(sampleText);
        while(m.find()) {
            blanks.add(m.group(1));
        }

        answers = new String[blanks.size()];
        input.setHint(blanks.get(0));
        headerTv.setText(blanks.size() +str1);
        footerTv.setText(str2 + blanks.get(0));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ok_btn) {
            System.out.print(input.getText().toString());
            if (input.getText().equals("") || input.getText().toString().isEmpty()) {
                Toast.makeText(this,"Please enter the required word before pressing \"OK\"",Toast.LENGTH_SHORT).show();
            } else {
                answers[position-1] = String.valueOf(input.getText());
                position++;
                if (position>blanks.size()) {
                    String completeStory = "";

                    for (int i = 0 ; i < blanks.size(); i++) {
                        completeStory = completeStory + myStrings[i] + " <b>" + answers[i] + "</b> ";
                    }

                    completeStory +=myStrings[blanks.size()];
                    System.out.println(completeStory);
                    Intent i = new Intent(this,ResultActivity.class);
                    i.putExtra("result",completeStory);
                    startActivity(i);
                    Toast.makeText(this,"ty for entering the values",Toast.LENGTH_SHORT).show();
                } else {
                    input.setText("");
                    input.setHint(blanks.get(position - 1));
                    headerTv.setText(blanks.size() - position + 1 + str1);
                    footerTv.setText(str2 + blanks.get(position - 1));
                }

            }
        }
    }
}

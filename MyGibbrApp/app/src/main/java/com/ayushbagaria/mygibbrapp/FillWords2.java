package com.ayushbagaria.mygibbrapp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FillWords2 extends AppCompatActivity implements View.OnClickListener {
    String[] myStrings;
    String sampleText;
    String delimiters;
    TextView myTextView;
    EditText myEditText;
    Button previous, next, submit;
    int position = 1;
    ArrayList<String> blanks;
    String[] answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_words2);
        myTextView = (TextView)findViewById(R.id.my_text);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myEditText = (EditText)findViewById(R.id.input);
        previous = (Button)findViewById(R.id.previous_btn);
        next = (Button)findViewById(R.id.next_btn);
        submit = (Button)findViewById(R.id.btn_submit_answers);
        previous.setOnClickListener(this);
        next.setOnClickListener(this);
        submit.setOnClickListener(this);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            sampleText = extras.getString("mySampleText");
        }

        delimiters =  "\\<[a-zA-Z-]*\\>";
        myStrings = sampleText.split(delimiters);
        String tempString = myStrings[0];

        for(int i = 1; i<myStrings.length; i ++) {
            System.out.println(myStrings[i]);
            System.out.println(tempString);
            tempString += i +"_____" +  myStrings[i];
        }
        System.out.println(tempString);
        myTextView.append(tempString);

        blanks = new ArrayList<>();
        Pattern p = Pattern.compile("\\<(.*?)\\>");

        Matcher m = p.matcher(sampleText);
        while(m.find()) {
            System.out.println(m.group(1));
            blanks.add(m.group(1));
        }
        previous.setEnabled(false);
        myEditText.setHint(position + " " + blanks.get(position - 1));
        answers = new String[blanks.size()];

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previous_btn : {
                System.out.println("previous");
                answers[(position-1)] = String.valueOf(myEditText.getText());
                if (position >= 2) {
                    --position;
                    myEditText.setHint(position + " " + blanks.get(position - 1));
                }
                if (position == 1) {
                    previous.setEnabled(false);
                    next.setEnabled(true);
                }

                myEditText.setText(answers[position-1]);
                if (answers[position-1] == null){
                    myEditText.setText("");
                }
                else myEditText.setText(answers[position-1]);
                break;
            }
            case R.id.next_btn : {
                System.out.println("next");
                answers[(position-1)] = String.valueOf(myEditText.getText());
                if (position <= blanks.size()) {
                    ++position;
                    myEditText.setHint(position +" " + blanks.get(position-1));
                }
                if (position == blanks.size()) {
                    next.setEnabled(false);
                }

                if (position >  1 && position <= blanks.size()){
                    previous.setEnabled(true);
                }
                if (answers[position-1] == null){
                    myEditText.setText("");
                }
                else myEditText.setText(answers[position-1]);
                break;
            }

            case R.id.btn_submit_answers : {
                answers[(position-1)] = String.valueOf(myEditText.getText());
                for (int i = 0 ; i< answers.length;i++){
                    System.out.println("asa" + answers[i] == null );
                    System.out.println(answers[i].isEmpty());
                    if (answers[i] == null || answers[i].equals("")) {
                        Toast.makeText(this, "Please enter all the answers then press \"Submit\" ", Toast.LENGTH_SHORT).show();
                        break;
                    }

                }

                String completeStory = "";

                for (int i = 0 ; i < blanks.size(); i++) {
                    completeStory = completeStory + myStrings[i] + " <b>" + answers[i] + "</b> ";
                }

                completeStory +=myStrings[blanks.size()];
                System.out.println(completeStory);
                Intent i = new Intent(this,ResultActivity.class);
                i.putExtra("result",completeStory);
                startActivity(i);
                Toast.makeText(this,"ty for entering all answers ",Toast.LENGTH_SHORT).show();
                break;
            }

            default : {
                break;
            }
        }
    }
}
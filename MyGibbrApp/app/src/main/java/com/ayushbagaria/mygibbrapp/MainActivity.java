package com.ayushbagaria.mygibbrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String[] myStrings;
    String sampleText;
    String delimiters;
    Button type1, type2;
    int position = 1;
    ArrayList<String> blanks;
    String[] answers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        type1 = (Button)findViewById(R.id.btn_type1);
        type2 = (Button)findViewById(R.id.btn_type2);
        type1.setOnClickListener(this);
        type2.setOnClickListener(this);
        Scanner s = new Scanner(getResources().openRawResource(R.raw.sample_text));
        try {
            while (s.hasNext()) {
                if (sampleText != null)
                    sampleText = sampleText + " "+ s.next();
                else  sampleText = s.next();
            }
        } finally {
            s.close();
        }
        System.out.println(sampleText);
        delimiters =  "\\<[a-zA-Z-]*\\>";
        myStrings = sampleText.split(delimiters);

        blanks = new ArrayList<>();
        Pattern p = Pattern.compile("\\<(.*?)\\>");

        Matcher m = p.matcher(sampleText);
        while(m.find()) {
            System.out.println(m.group(1));
            blanks.add(m.group(1));
        }


        answers = new String[blanks.size()];

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_type1 : {
                Intent i = new Intent(this, FillWords1.class);
                i.putExtra("mySampleText",sampleText);
                startActivity(i);
                break;
            }
            case R.id.btn_type2 : {
                Intent i = new Intent(this, FillWords2.class);
                i.putExtra("mySampleText",sampleText);
                startActivity(i);
                break;
            }
            default : {
                break;
            }
        }
    }
}
